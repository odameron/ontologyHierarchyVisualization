#! /usr/bin/env bash

if [ ! $# -eq "1" ]  # Script invoked with wrong number of args
then
  echo "Usage: `basename $0` path/to/ontology"
  exit $E_OPTERROR        # Exit and explain usage, if no argument(s) given.
fi  

${FUSEKI_HOME}/fuseki-server --file=$1 /ontologyHierarchy
