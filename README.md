# ontology Hierarchy Visualization

Simple functions for generating a graphical representation of the hierarchy of a (fraction of an) ontology


# Dependencies

- Python3
- [rdflib](https://github.com/RDFLib/rdflib)
- [networkx](https://networkx.org/)

# Usage

## Set up an Apache fuseki endpoint for the ontology

Run the following from a terminal (and do not close the terminal until you are done)

```bash
./startFusekiWithOntology.bash path/to/ontology.owl
```


### Call the hierarchy visualization script

The `ontologyPrefix` parameter is used at the beginning of the dot file, and as prefix for the classes identifiers in the diagram.

```bash
./ontologyHierarchyVisualization.py ontologyPrefix classID1 [ClassID2...]
```

So far, the diagram is generated in the `example` subdirectory (cf. Todo section)


# Todo

- [ ] specify diagram file path as an argument
- [ ] handle not only `rdfs:subClassOf` but also other relations (e.g. for Gene Ontology)
- [x] `createGraphAncestors(classID)`
- [ ] `createGraphDescendants(classID)`
    - [ ] update
- [ ] `displayHierarchyFromRoot(classID)`
- add attributes to customize on a class-based level
    - [ ] node size
    - [ ] node color
- export
    - [x] dot
    - [ ] gexf
-  [ ] add wrapper script to generate a png, pdf,...
- [ ] `exploreNeighborhood(entityID)`
