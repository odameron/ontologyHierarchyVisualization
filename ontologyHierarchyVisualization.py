#! /usr/bin/env python3

import networkx as nx
import rdflib
from networkx.drawing.nx_agraph import write_dot
from SPARQLWrapper import SPARQLWrapper, JSON
import sys

endpointURL = "http://localhost:3030/ontologyHierarchy/query"

###
### HPO
###
#
# ./startFusekiWithOntology.bash ~/ontology/humanPhenotypeOntology/hp.owl
#
# ./ontologyHierarchyVisualization.py hpo 0000365
# ./ontologyHierarchyVisualization.py hpo 0000365 0000360
# ./ontologyHierarchyVisualization.py hpo 0000365 0031704
#
# ./ontologyHierarchyVisualization.py hpo 0011003 0000545
# ./ontologyHierarchyVisualization.py hpo 0040064 0001999



###
### GO
###
#
# ./startFusekiWithOntology.bash ~/ontology/geneOntology/go-latest.owl
#
# ./ontologyHierarchyVisualization.py go 0006001 0019302
# ./ontologyHierarchyVisualization.py go 0006001 0019303


sparqlPrefixes = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX dct: <http://purl.org/dc/terms/>
"""


def formatURI(classID, uriPrefix):
    if uriPrefix.startswith("http"):
        return "<" + uriPrefix + classID + ">"
    return uriPrefix + ":" + classID


def getLocalName(uri, uriPrefix):
    if uri.startswith("<"):
        return uri[1:-1].replace(uriPrefix, "")
    if uri.startswith("http"):
        return uri.replace(uriPrefix, "")
    if uriPrefix.endswith(":"):
        if uri.startswith(uriPrefix):
            return uri.replace(uriPrefix, "")
    if uri.startswith(uriPrefix + ":"):
        return uri.replace(uriPrefix + ":", "")
    return uri


def retrieveURIprefixesLocal(rdfGraph, localID):
    sparqlQuery = sparqlPrefixes + """
SELECT DISTINCT ?entity
WHERE {
  { ?entity ?p ?o . }
  union
  { ?s ?p ?entity . }
  FILTER (isIRI(?entity) && strends(str(?entity),\"""" + localID + """\"))
}
"""
    qres = rdfGraph.query(sparqlQuery)
    uriPrefixes = []
    for row in qres:
        uriPrefixes.append(str(row[0]).replace(localID, ""))
    return uriPrefixes


def retrieveURIprefixes(endpoint, localID):
    sparqlQuery = sparqlPrefixes + """
SELECT DISTINCT ?entity
WHERE {
  { ?entity ?p ?o . }
  union
  { ?s ?p ?entity . }
  FILTER (isIRI(?entity) && strends(str(?entity),\"""" + localID + """\"))
}
"""
    endpoint.setQuery(sparqlQuery)
    endpoint.setReturnFormat(JSON)
    results = endpoint.query().convert()
    uriPrefixes = []
    for result in results["results"]["bindings"]:
        uriPrefixes.append(str(result["entity"]["value"]).replace(localID, ""))
    return uriPrefixes


def createGraphDescendants(rdfGraph, classID, uriPrefix, filePath=""):
    if filePath == "":
        filePath = classID + "-descendants.dot"
    sparqlQuery = sparqlPrefixes + """
SELECT ?class ?classLabel ?superClass ?superClassLabel
WHERE {
  ?class rdf:type owl:Class .
  OPTIONAL { ?class rdfs:label ?classLabel . }
  FILTER NOT EXISTS {
    ?class owl:deprecated "true"^^xsd:boolean .
  }
  ?class rdfs:subClassOf ?superClass .
  ?superClass rdf:type owl:Class .
  OPTIONAL { ?superClass rdfs:label ?superClassLabel . }
  FILTER NOT EXISTS {
    ?superClass owl:deprecated "true"^^xsd:boolean .
  }

  ?superClass rdfs:subClassOf* """ + formatURI(classID, uriPrefix) + """ .
}
"""
    #print(sparqlQuery)
    qres = rdfGraph.query(sparqlQuery)
    G = nx.DiGraph(rankdir="BT")
    for row in qres:
        #print("{}\t{}\t->\t{}\t{}".format(getLocalName(row[0], uriPrefix), row[1], getLocalName(row[2], uriPrefix), row[3]))
        nodeSrc = getLocalName(row[0], uriPrefix)
        nodeDest = getLocalName(row[2], uriPrefix)
        G.add_node(nodeSrc, shape="box", label=row[1] + "\n" + nodeSrc)
        G.add_node(nodeDest, shape="box", label=row[3] + "\n" + nodeDest)
        G.add_edge(nodeSrc, nodeDest)
    write_dot(G, filePath)


def createGraphAncestorsLocal(rdfGraph, classID, uriPrefix, filePath=""):
    if filePath == "":
        filePath = classID + "-ancestors.dot"
    sparqlQuery = sparqlPrefixes + """
SELECT ?class ?classLabel ?superClass ?superClassLabel
WHERE {
  ?class rdf:type owl:Class .
  OPTIONAL { ?class rdfs:label ?classLabel . }
  FILTER NOT EXISTS {
    ?class owl:deprecated "true"^^xsd:boolean .
  }
  ?class rdfs:subClassOf ?superClass .
  ?superClass rdf:type owl:Class .
  OPTIONAL { ?superClass rdfs:label ?superClassLabel . }
  FILTER NOT EXISTS {
    ?superClass owl:deprecated "true"^^xsd:boolean .
  }

  #?superClass rdfs:subClassOf* """ + formatURI(classID, uriPrefix) + """ .
  """ + formatURI(classID, uriPrefix) + """ rdfs:subClassOf* ?class .
}
"""
    qres = rdfGraph.query(sparqlQuery)
    G = nx.DiGraph(rankdir="BT")
    for row in qres:
        #print("{}\t{}\t->\t{}\t{}".format(getLocalName(row[0], uriPrefix), row[1], getLocalName(row[2], uriPrefix), row[3]))
        nodeSrc = getLocalName(row[0], uriPrefix)
        nodeDest = getLocalName(row[2], uriPrefix)
        G.add_node(nodeSrc, shape="box", label=row[1] + '\n' + nodeSrc)
        G.add_node(nodeDest, shape="box", label=row[3] + '\n' + nodeDest)
        G.add_edge(nodeSrc, nodeDest)
    write_dot(G, filePath)




def createGraphAncestors(endpoint, classesID, uriPrefix, filePath="", displayPrefix=":"):
    if len(classesID) == 0:
        return
    if filePath == "":
        filePath = "-".join(classesID) + "-ancestors.dot"
    classesOfInterest = "<" + uriPrefix + classesID[0] + ">"
    for classID in classesID[1:]:
        classesOfInterest += " <" + uriPrefix + classID + ">"
    sparqlQuery = sparqlPrefixes + """
SELECT DISTINCT ?class ?classLabel ?superClass ?superClassLabel
WHERE {
  VALUES ?classOfInterest { """ + classesOfInterest + """ } .
  ?class rdf:type owl:Class .
  OPTIONAL { ?class rdfs:label ?classLabel . }
  FILTER NOT EXISTS {
    ?class owl:deprecated "true"^^xsd:boolean .
  }
  ?class rdfs:subClassOf ?superClass .
  ?superClass rdf:type owl:Class .
  OPTIONAL { ?superClass rdfs:label ?superClassLabel . }
  FILTER NOT EXISTS {
    ?superClass owl:deprecated "true"^^xsd:boolean .
  }

  ?classOfInterest rdfs:subClassOf* ?class .
}
"""
    print("??? " + sparqlQuery + "\n")
    endpoint.setQuery(sparqlQuery)
    endpoint.setReturnFormat(JSON)
    results = endpoint.query().convert()
    G = nx.DiGraph(rankdir="BT")
    for result in results["results"]["bindings"]:
        #print("{}\t{}\t->\t{}\t{}".format(getLocalName(row[0], uriPrefix), row[1], getLocalName(row[2], uriPrefix), row[3]))
        nodeSrc = getLocalName(result["class"]["value"], uriPrefix)
        nodeDest = getLocalName(result["superClass"]["value"], uriPrefix)
        if nodeSrc in classesID:
            G.add_node(nodeSrc, shape="box", label=result["classLabel"]["value"] + '\n' + displayPrefix + nodeSrc, color='red')
        else:
            G.add_node(nodeSrc, shape="box", label=result["classLabel"]["value"] + '\n' + displayPrefix + nodeSrc)
        if nodeDest in classesID:
            G.add_node(nodeDest, shape="box", label=result["superClassLabel"]["value"] + '\n' + displayPrefix + nodeDest, color='red')
        else:
            G.add_node(nodeDest, shape="box", label=result["superClassLabel"]["value"] + '\n' + displayPrefix + nodeDest)
        G.add_edge(nodeSrc, nodeDest)
    write_dot(G, filePath)



def createGraphAllAncestors(endpoint, classesID, uriPrefix, filePath="", displayPrefix=":"):
    sparqlQuery = sparqlPrefixes + """
SELECT DISTINCT ?class ?classLabel ?superClass ?superClassLabel
WHERE {
  ?class rdf:type owl:Class .
  OPTIONAL { ?class rdfs:label ?classLabel . }
  FILTER NOT EXISTS {
    ?class owl:deprecated "true"^^xsd:boolean .
  }
  ?class rdfs:subClassOf ?superClass .
  ?superClass rdf:type owl:Class .
  OPTIONAL { ?superClass rdfs:label ?superClassLabel . }
  FILTER NOT EXISTS {
    ?superClass owl:deprecated "true"^^xsd:boolean .
  }

  ?classOfInterest rdfs:subClassOf* ?class .
}
"""
    endpoint.setQuery(sparqlQuery)
    endpoint.setReturnFormat(JSON)
    results = endpoint.query().convert()
    G = nx.DiGraph(rankdir="BT")
    for result in results["results"]["bindings"]:
        #print("{}\t{}\t->\t{}\t{}".format(getLocalName(row[0], uriPrefix), row[1], getLocalName(row[2], uriPrefix), row[3]))
        nodeSrc = getLocalName(result["class"]["value"], uriPrefix)
        nodeDest = getLocalName(result["superClass"]["value"], uriPrefix)
        if nodeSrc in classesID:
            G.add_node(nodeSrc, shape="box", label=result["classLabel"]["value"] + '\n' + displayPrefix + nodeSrc, color='red')
        else:
            G.add_node(nodeSrc, shape="box", label=result["classLabel"]["value"] + '\n' + displayPrefix + nodeSrc)
        if nodeDest in classesID:
            G.add_node(nodeDest, shape="box", label=result["superClassLabel"]["value"] + '\n' + displayPrefix + nodeDest, color='red')
        else:
            G.add_node(nodeDest, shape="box", label=result["superClassLabel"]["value"] + '\n' + displayPrefix + nodeDest)
        G.add_edge(nodeSrc, nodeDest)
    write_dot(G, filePath)


if __name__ == "__main__":
    rdflibFormat = {}
    rdflibFormat['.owl'] = 'xml'
    rdflibFormat['.ttl'] = 'ttl'

    if len(sys.argv) < 2:
        print("Error: Wrong number of arguments")
        print("Usage: " + sys.argv[0] + " ontologyPrefix (ClassOfInterest)+")
        sys.exit(1)

    #datasetPath = sys.argv[1]
    #prefixOfInterest = sys.argv[2]
    ontologyPrefix = sys.argv[1]
    classesOfInterest = sys.argv[2:]
    classOfInterest = classesOfInterest[0]
    #datasetPath = 'example/ontologyHierarchySimple.owl'
    #classOfInterest = 'ClassB'
    #classOfInterest = 'ClassJ'
    prefixOfInterest = ''
    #prefixOfInterest = 'http://purl.bioontology.org/ontology/NCBITAXON/'



    #datasetPath = '../../ontology/apid/mi.owl'
    #classOfInterest = '0001'
    #prefixOfInterest = 'http://purl.obolibrary.org/obo/MI_'

    #rdfGraph = rdflib.Graph()
    #rdfGraph.load(datasetPath)

    sparqlEndpoint = SPARQLWrapper(endpointURL)

    if prefixOfInterest == '':
        candidatePrefixes = retrieveURIprefixes(sparqlEndpoint, classOfInterest)
        if len(candidatePrefixes) == 0:
            print("No entity matching " + classOfInterest)
            sys.exit(1)
        if len(candidatePrefixes) > 1:
            print("Multiple entities matching " + classOfInterest + ":")
            for currentPrefix in candidatePrefixes:
                print("- " + currentPrefix)
            print("")
            print("Please specify prefix")
            sys.exit(2)
        prefixOfInterest = candidatePrefixes[0]

    #createGraphDescendants(rdfGraph, classOfInterest, prefixOfInterest, 'example/' + classOfInterest + '-descendants.dot')
    #createGraphAncestors(rdfGraph, classOfInterest, prefixOfInterest, 'example/' + classOfInterest + '-ancestors.dot')
    createGraphAncestors(sparqlEndpoint, classesOfInterest, prefixOfInterest, 'example/' + ontologyPrefix + '_all-ancestors.dot', displayPrefix=ontologyPrefix + ":")
    #createGraphAllAncestors(sparqlEndpoint, classesOfInterest, prefixOfInterest, 'example/' + ontologyPrefix + '_all-ancestors.dot', displayPrefix=ontologyPrefix + ":")
